const path      = require('path');
const gulp      = require('gulp');
const rename    = require('gulp-rename');

const vendorPath = path.join( __dirname, 'public', 'vendor');

gulp.task('copy:js', function() {
  return gulp.src(['./bower_components/**/*.min.js', './bower_components/socket.io-client/*.js  ']).pipe(rename({ dirname: 'scripts' })).pipe(gulp.dest(vendorPath));
});

gulp.task('copy:css', function() {
  return gulp.src('./bower_components/**/*.min.css').pipe(rename({ dirname: 'styles' })).pipe(gulp.dest(vendorPath));
});

gulp.task('copy:fonts', function() {
  return gulp.src('./bower_components/fonts/**/*.').pipe(rename({ dirname: 'fonts' })).pipe(gulp.dest(vendorPath));
});

gulp.task('copy', [ 'copy:js', 'copy:css', 'copy:fonts' ]);
gulp.task('default', ['copy:js']);

gulp.task('prod', ['copy']);