angular.module('module', ['btford.socket-io'])
  .controller('DefaultCtrl', function($scope, $timeout, $element) {
    $scope.updateAlways = function() {
      return true;
    };
    
    $scope.setLock = function(card) {
      card.lock = true;
    };
    
    $scope.unsetLock = function(card) {
      card.lock = false;
    };
    
    $scope.disable = function() {
      console.log(arguments);
    };
    
    $scope.enable = function() {
      console.log(arguments);
    };
  })
  .factory('socket', function(socketFactory) {
    return socketFactory();
  })
  .directive('customNamespace', function() {
    return {
      scope: {
        customNamespaceData: '=?'
      },
      restrict: 'A',
      require: ['^^?customNamespace', 'customNamespace'],
      link: {
        pre: function(scope, el, attr, ctrls) {
          var providedCtrls = ctrls
            .filter(function(ctrl) {
              return !!ctrl;
            });
          
          var namespace = providedCtrls.map(function(cn) {
              return cn.getNamespace();
            }).join('.');
          
          var customData = providedCtrls.reduce(function(data, cn) {
            return angular.extend({}, data, cn.getNamespaceData());
          }, {});
          
          scope.$namespace = namespace;
          scope.$customNamespaceData = customData;
        }
      },
      controller: function($scope, $attrs, $parse) {
        this.getNamespace = function() {
          return $scope.$namespace || $attrs.customNamespace;
        };
        
        this.getNamespaceData = function() {
          return angular.extend({}
            , $scope.$customNamespaceData
            , $parse($attrs.customNamespaceData)($scope));
        };
        
        $scope.doStuff = function() {
          return $attrs.customNamespaceData;
        };
      }
    };
  })
  .directive('customEmit', function($parse, socket) {
    return {
      restrict: 'A',
      require: ['ngModel', '^customNamespace'],
      link: function(scope, el, attr, ctrls) {
        var ngModel = ctrls.shift();
        var cn = ctrls.shift();
        var eventRoot = cn.getNamespace();
        var event = eventRoot.concat('.').concat(attr.customEmit);
        
        if (!attr.customEmit) {
          throw new Error("You need to provide the event to be emitted when the trigger hits");
        }
        
        var attrEmit = attr.customEmit;
        var trigger;
        
        if (!attr.customEmitTrigger) {
          trigger = function() { return true; };
        } else {
          var attrTrigger = attr.customEmitTrigger;
          trigger = attrTrigger in scope ? $parse(attrTrigger)(scope) : attrTrigger;
        }
        
        var watchAndApplyOn = function(exprToWatch, predicate, then, otherwise) {
          scope.$watch(exprToWatch, function(n, o) {
            if (n === o) {
              return;
            }
            
            if (!predicate(n, o, el)) {
              return otherwise && otherwise();
            }
            
            then();
          });
        };
        
        var bindHandler = function(event, then, otherwise) {
          el.on(event, then);
          
          if (otherwise) {
            el.off(event, otherwise);
          }
        };
        
        scope.$emitLock = function(attr) {
          console.log("Sure", eventRoot.concat('.').concat('attr'));
        };  //socket.emit.bind(null, eventRoot.concat('.').concat('locked'));
        
        scope.$emitUnlock = function(attr) {
          console.log("Whatever", eventRoot.concat('.').concat('attr'));
        };  //socket.emit.bind(null, eventRoot.concat('.').concat('unlocked'));
        
        var canEmit = true;
        
        if (attr.customEmitIf) {
          canEmit = $parse(attr.customEmitIf)(scope);
          
          attr.$observe('customEmitIf', function(val) {
            canEmit = $parse(val)(scope);
          });
        }
        
        if (attr.customEmitLockOn) {
          var attrLockOn = attr.customEmitLockOn;
          
          var lock = function() {
            socket.emit(eventRoot.concat('.').concat('locked'));
          };
          
          var unlock = function() {
            socket.emit(eventRoot.concat('.').concat('unlocked'));
          };
          
          var tryParseLockOn = $parse(attrLockOn)(scope);
          var lockOn = $parse(attrLockOn)(scope) === undefined ? attrLockOn : tryParseLockOn;
          
          if (typeof lockOn === 'function') {
            watchAndApplyOn(function() { return el; }, lockOn, lock, unlock);
          } else if (typeof lockOn === 'string') {
            bindHandler(lockOn, lock, unlock);
          } else if (typeof lockOn === 'boolean') {
            var prev = lockOn;
            
            attr.$observe('customEmitLockOn', function(val) {
              var current = $parse(val)(scope);
              
              if (current === prev) {
                return;
              }
              
              prev = current;
              
              if (current) {
                lock();
              } else {
                unlock();
              }
            });
          } else {
            throw new Error("Provided locker must be either a regular attribute specifying a DOM event or a scoped function");
          }
        }
        
        var getModel = function() {
          var emitData = {};
          emitData[attr.customNamespace] = ngModel.$modelValue;
          return angular.extend({}, cn.getNamespaceData(), emitData);
        };
        
        if (typeof trigger === 'function') {
          watchAndApplyOn(function() { return ngModel.$modelValue; }, function(n, o) {
            return canEmit && trigger(n, o);
          }, function() {
            socket.emit(event, getModel());
          });
        } else if (typeof trigger === 'string') {
          bindHandler(trigger, function() {
            if (!canEmit) {
              return;
            }
            
            socket.emit(event, getModel());
          });
        } else {
          throw new Error("Provided trigger must be either a regular attribute specifying a DOM event or a scoped function");
        }
      }
    };
  })
  .directive('customReceive', function(socket, $parse) {
    return {
      restrict: 'A',
      require: ['?ngModel', '^customNamespace'],
      link: function(scope, el, attr, ctrls) {
        var ngModel = ctrls.shift();
        var cn = ctrls.shift();
        var eventRoot = cn.getNamespace();
        var event = eventRoot.concat('.').concat(attr.customReceive);
        
        var onLock = attr.customReceiveOnLock || '';
        var onUnlock = attr.customReceiveOnUnlock || '';
        
        socket.on(event, function(data) {
          scope[event] = 'receiving';
          var content = typeof data === 'object' ? data[attr.customNamespace] : data;
          
          if (ngModel) {
            ngModel.$setViewValue(content);
            ngModel.$render()
          } else {
            el.html(content);
          }
        });
        
        socket.on(eventRoot.concat('.').concat('locked'), function() {
          scope.$eval(onLock);
        });
        
        socket.on(eventRoot.concat('.').concat('unlocked'), function() {
          scope.$eval(onUnlock);
        });
      }
    };
  });