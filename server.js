const path      = require('path');
const http      = require('http');
const express   = require('express');
const socketio  = require('socket.io');

const app     = express();
const router  = new express.Router();
const server  = http.createServer(app);
const io      = socketio.listen(server);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public', 'index.html')));

const port = process.env.PORT || 3000;
const ip = process.env.IP || '0.0.0.0';

server.listen(port, ip, console.log.bind(console, `App running at: ${ip}:${port}`));


/* Just Some Socket Things */
io.on('connection', function(socket) {
  socket.on('space.integrations.trello.card.name1.changed', function(data) {
    console.log(data);
    socket.broadcast.emit('space.integrations.trello.card.name1.changed', data);
  });
  
  socket.on('space.integrations.trello.card.name2.changed', function(data) {
    console.log(data);
    socket.broadcast.emit('space.integrations.trello.card.name2.changed', data);
  });
  
  socket.on('space.integrations.trello.card.name3.changed', function(data) {
    console.log(data);
    socket.broadcast.emit('space.integrations.trello.card.name3.changed', data);
  });
  
  socket.on('space.integrations.trello.card.name3.locked', function(data) {
    console.log(data);
    socket.broadcast.emit('space.integrations.trello.card.name3.locked', data);
  });
  
  socket.on('space.integrations.trello.card.name3.unlocked', function(data) {
    console.log(data);
    socket.broadcast.emit('space.integrations.trello.card.name3.unlocked', data);
  });
});
